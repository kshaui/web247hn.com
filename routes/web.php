<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/language', 'Controller@switchLanguage')->name('switchLanguage');
Route::get('/test', 'Web\TestController@index')->name('web.test');

Route::name('web.')->group(function () {
	//home
    Route::name('home.')->group(function () {
	    foreach (config('app.languages') as $key => $value) {//sinh ra các url của home (theo local ngoại trừ locale default)
			if($key == config('app.fallback_locale'))
				Route::get('/', 'Web\HomeController@index')->name($key);
			else
				Route::get('/'.$key, 'Web\HomeController@index')->name($key);
		}
	});

    //page
    Route::name('pages.')->group(function () {
	    foreach (config('app.languages') as $key => $value) {
			if($key == 'vi')//fix url cho page với tiếng Việt
				Route::get('/trang/{slug}.html','Web\PageController@show')->name($key);
			elseif($key == 'en')//fix url cho page với tiếng Anh
				Route::get('/page/{slug}.html','Web\PageController@show')->name($key);
			else//các ngôn ngữ khác để mặc định
				Route::get('/'.$key.'/page/{slug}.html','Web\PageController@show')->name($key);
		}
	});

    //news
    Route::name('news.')->group(function () {
	    foreach (config('app.languages') as $key => $value) {
			if($key == 'vi')
				Route::get('/tin-tuc/{slug}.html','Web\NewsController@show')->name($key);
			elseif($key == 'en')
				Route::get('/news/{slug}.html','Web\NewsController@show')->name($key);
			else
				Route::get('/'.$key.'/news/{slug}.html','Web\NewsController@show')->name($key);
		}
	});

    //news_categories
    Route::name('news_categories.')->group(function () {
	    foreach (config('app.languages') as $key => $value) {
			if($key == 'vi')
				Route::get('/tin-tuc/{slug?}','Web\NewsController@index')->name($key);
			elseif($key == 'en')
				Route::get('/news/{slug?}','Web\NewsController@index')->name($key);
			else
				Route::get('/'.$key.'/news/{slug?}','Web\NewsController@index')->name($key);
		}
	});

    //products
    Route::name('products.')->group(function () {
	    foreach (config('app.languages') as $key => $value) {
			if($key == 'vi')
				Route::get('{slug}.html','Web\ProductController@show')->name($key);
			else
				Route::get('/'.$key.'/{slug}.html','Web\ProductController@show')->name($key);
		}
	});

    //products_categories
    Route::name('products_categories.')->group(function () {
	    foreach (config('app.languages') as $key => $value) {
			if($key == 'vi')
				Route::get('/san-pham/{slug?}','Web\ProductController@index')->name($key);
			elseif($key == 'en')
				Route::get('/products/{slug?}','Web\ProductController@index')->name($key);
			else
				Route::get('/'.$key.'/products/{slug?}','Web\ProductController@index')->name($key);
		}
	});
});