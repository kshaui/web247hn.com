<?php
Route::group(['prefix' => 'admin'], function() {
    Route::get('/', 'Admin\AdminController@index')->middleware('auth-admin')->name('admin.dashboard');
    Route::get('/login','Admin\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login','Admin\LoginController@login')->name('admin.login.submit');
    Route::get('logout','Admin\LoginController@logout')->name('admin.logout');
    Route::get('/permission-denied','Admin\Controller@permissionDenied')->name('admin.permission.denied');
    // route cho admin user admin
    Route::group(['prefix' => 'admin_users','middleware'=>'auth-admin'], function() {
        Route::get('change-password','Admin\AdminUserController@changePassword')->name('admin.admin_user.changePassword');
        Route::post('change-password','Admin\AdminUserController@postChangePassword');
    });
    Route::resource('admin_users','Admin\AdminUserController',['middleware'=>'auth-admin']);
    //route cho settings
    Route::group(['prefix' => 'settings','middleware'=>'auth-admin'], function() {
        Route::match(['GET', 'POST'],'/general','Admin\SettingController@general')->name('admin.setting.general');
        Route::match(['GET', 'POST'],'/home','Admin\SettingController@home')->name('admin.setting.home');
        Route::match(['GET', 'POST'],'/seo','Admin\SettingController@seo')->name('admin.setting.seo');
    });
    //route cho media
    Route::group(['prefix' => 'media','middleware'=>'auth-admin'], function() {
        Route::get('library','Admin\MediaController@library')->name('admin.media.library');
        Route::post('store', 'Admin\MediaController@store')->name('admin.media.store');
        Route::post('update', 'Admin\MediaController@update')->name('admin.media.update');
        Route::post('search','Admin\MediaController@search')->name('admin.media.search');
    });
    // route cho ajax
    Route::group(['prefix' => 'ajax'], function() {
        Route::post('get-slug', 'Admin\Controller@getSlug');
        Route::post('delete-all', 'Admin\Controller@deleteAll');
        Route::post('trash-all', 'Admin\Controller@trashAll');
        Route::post('deactive-all', 'Admin\Controller@deactiveAll');
        Route::post('save-one', 'Admin\Controller@saveOne');
        Route::post('save-all', 'Admin\Controller@saveAll');
        Route::post('relate-suggest', 'Admin\Controller@relateSuggest');
        Route::post('pins', 'Admin\Controller@pins');
        Route::post('reset-password', 'Admin\AjaxController@resetPassword');
    });
    Route::resource('system_logs','Admin\SystemLogController',['middleware'=>'auth-admin']);

    //Các module resource
    Route::resource('products_categories','Admin\ProductsCategoryController',['middleware'=>'auth-admin']);
    Route::resource('products','Admin\ProductController',['middleware'=>'auth-admin']);
    Route::resource('news_categories','Admin\NewsCategoryController',['middleware'=>'auth-admin']);
    Route::resource('news','Admin\NewsController',['middleware'=>'auth-admin']);
    Route::resource('pages','Admin\PageController',['middleware'=>'auth-admin']);
    Route::resource('partners','Admin\PartnerController',['middleware'=>'auth-admin']);

});