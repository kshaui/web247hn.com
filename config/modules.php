<?php

return [
    'module' => [
        'products_categories' => ['access','create','edit','delete'],
        'products' => ['access','create','edit','delete'],
        'news_categories' => ['access','create','edit','delete'],
        'news' => ['access','create','edit','delete'],
        'pages' => ['access','create','edit','delete'],
        'partners' => ['access','create','edit','delete'],
        'admin_users' => ['access','create','edit','delete'],
        'settings' => [
            'access',
            'general',
            'home',
            'seo',
        ],
        'system_logs' => ['access','create','edit','delete'],
    ],

    'name' => [
        'products_categories'=>'Danh mục sản phẩm',
        'products'=>'Sản phẩm',
        'news_categories'=>'Danh mục tin tức',
        'news'=>'Tin tức',
        'pages'=>'Trang',
        'partners'=>'Đối tác',
        'admin_users'=>'Tài khoản quản trị',
        'settings'=>'Cấu hình',
        'system_logs'=>'Logs hệ thống',

        'access'=>'Truy cập',
        'create'=>'Thêm',
        'edit'=>'Sửa',
        'delete'=>'Xóa',

        'general'=>'Cấu hình chung',
        'home'=>'Cấu hình trang chủ',
        'seo'=>'Cấu hình SEO',
    ],

    'icon' => [
        'products_categories'=>'fa-bars',
        'products'=>'fa-archive',
        'news_categories'=>'fa-bars',
        'news'=>'fa-newspaper-o',
        'pages'=>'fa-file-text',
        'partners'=>'fa-cubes',
        'admin_users'=>'fa-users',
        'settings'=>'fa-cogs',
        'system_logs'=>'fa-repeat',
    ]
];
