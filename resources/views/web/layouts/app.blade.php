<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{url('/')}}/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
    <link rel="stylesheet" type="text/css" href="/css/app.css"/>
    <script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
    @include('web.layouts.seo')
    @yield('head')
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    @include('web.layouts.adminbar')
	<form action="" class="form-lang" method="get">
        <p style="display: inline;padding-right: 10px;">{!!__('Ngôn ngữ')!!} ({{App::getLocale()}})</p>
        <select name="url" onchange='window.location.href = this.value;'>
            @foreach($switch_languages as $key=>$value)
                <option value="{{$value ?? route('web.home.'.$key)}}" {{ App::getLocale() == $key ? 'selected' : '' }}>{!! __(config('app.languages')[$key]) !!}</option>
            @endforeach
        </select>
    </form>
    <hr>

    @yield('content')
    @yield('foot')
</body>
</html>