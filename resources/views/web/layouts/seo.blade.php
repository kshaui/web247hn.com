<title>@if(!empty($meta_seo['title'])){{$meta_seo['title']}}@else{{''}}@endif</title>
@if(!empty($meta_seo['description']))
<meta name="description" content="{{$meta_seo['description']}}"/>
@endif
@if(!empty($meta_seo['robots']))
<meta name="robots" content="{{$meta_seo['robots']}}"/>
@endif
@if(!empty($meta_seo['switch_languages']))
@php $og_url = $meta_seo['switch_languages'][App::getLocale()]; @endphp
<link rel="canonical" href="{!!$og_url!!}" />
@foreach($meta_seo['switch_languages'] as $key=>$value)
@if($key != App::getLocale() && $value)
<link rel="alternate" hreflang="{{$key}}" href="{!!$value!!}" />
@endif
@endforeach
@elseif(!empty($meta_seo['url']))
@php $og_url = $meta_seo['url']; @endphp
<link rel="canonical" href="{{$meta_seo['url']}}" />
@endif
<meta property="og:site_name" content="{{ config('app.name') }}" />
<meta property="og:type" content="@if(!empty($meta_seo['type'])){{$meta_seo['type']}}@else{{'website'}}@endif" />
<meta property="og:title" content="@if(!empty($meta_seo['title'])){{$meta_seo['title']}}@else{{''}}@endif" />
@if(!empty($meta_seo['description']))
<meta property="og:description" content="{{$meta_seo['description']}}" />
@endif
@if(!empty($og_url))
<meta property="og:url" content="{{$og_url}}" />
@endif
@if(!empty($meta_seo['image']))
<meta property="og:image" content="{{$meta_seo['image']}}" />
@endif