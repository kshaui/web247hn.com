<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Administrator Managerment</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{!! url('/template-admin/css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! url('/template-admin/css/font-awesome.min.css') !!}" rel="stylesheet">
    <link href="{!! url('/template-admin/css/daterangepicker.css') !!}" rel="stylesheet">
    <link href="{!! url('/js/fancybox/jquery.fancybox.css') !!}" rel="stylesheet">
    <link href="{!! url('/template-admin/vendors/dropzone/dist/min/dropzone.min.css') !!}" rel="stylesheet">
    <link href="{!! url('/template-admin/css/custom.min.css') !!}" rel="stylesheet">
    <link href="{!! url('/template-admin/css/style1.css') !!}" rel="stylesheet">
    <link href="{!! url('/template-admin/css/media.css') !!}" rel="stylesheet">
</head>
<body class="media-body" style="background: #fff; overflow-y: auto;">
<div class="row">
    <div class="col-md-12">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="@if($view=='add'){{'active'}}@endif"><a href="#media-main-add" id="media-tab-add" role="tab" data-toggle="tab" aria-expanded="@if($view=='add'){{'true'}}@else{{'false'}}@endif">{{__('Thêm mới')}}</a></li>
                <li role="presentation" class="@if($view=='list'){{'active'}}@endif"><a href="#media-main-list" role="tab" id="media-tab-list" data-toggle="tab" aria-expanded="@if($view=='list'){{'true'}}@else{{'false'}}@endif">{{__('Thư viện')}}</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade @if($view=='add'){{'active in'}}@endif" id="media-main-add">
                    <div class="x_content">
                        <p>{{__('Chọn hoặc kéo file cần tải lên vào khung bên dưới')}}</p>
                        <form action="{!! route('admin.media.store') !!}" class="dropzone" id="sudoUploadForm">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="dz-custom dz-message">{{__('Chọn file để tải lên')}}</div>
                        </form>
                        <br />
                        <br />
                        <br />
                        <br />
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade @if($view=='list'){{'active in'}}@endif" id="media-main-list">
                    <div id="media-list">
                        <div id="media-list-control">
                            <input type="text" id="media-search-name" placeholder="{{__('Tìm theo tên')}} ...">
                            <div class="media-top-pagination clearfix">{{ $data->links() }}</div>
                        </div>
                        <div id="media-list-wrap">
                            @include('admin.media.list')
                        </div>
                        <div class="media-bottom-pagination clearfix">{{ $data->links() }}</div>
                    </div>
                    <div id="media-info">
                        <div class="media-info-img"></div>
                        <form class="media-info-form" action="#" method="post">
                            <input type="hidden" id="media-setting-id" value="0">
                            <p class="media-setting-title">{{__('Cập nhật thông tin')}}</p>
                            <label class="media-setting">
                                <span class="name">{{__('Tiêu đề')}}</span>
                                <input class="field" type="text" id="media-setting-title" value="">
                            </label>
                            <label class="media-setting">
                                <span class="name">{{__('Mô tả')}}</span>
                                <textarea class="field" id="media-setting-caption"></textarea>
                            </label>
                            <label class="media-setting">
                                <span class="name">&nbsp;</span>
                                <button id="media-setting-btn" class="btn btn-info btn-sm" style="margin: 10px 0px;">{{__('Cập nhật')}}</button>
                            </label>
                        </form>
                    </div>
                </div>
            </div>
            <div id="media-action">
                <button id="media-chose" disabled>{{$text}}</button>
            </div>
        </div>
    </div>
</div>

<script src="{!! url('/template-admin/js/jquery.min.js') !!}"></script>
<script src="{!! url('/template-admin/js/bootstrap.min.js') !!}"></script>
<script src="{!! url('/template-admin/js/fastclick.js') !!}"></script>
<script src="{!! url('/template-admin/js/nprogress.js') !!}"></script>
<script src="{!! url('/template-admin/js/moment.min.js') !!}"></script>
<script src="{!! url('/template-admin/vendors/dropzone/dist/min/dropzone.min.js') !!}"></script>
<script src="{!! url('/template-admin/js/jquery.sortable.min.js') !!}"></script>
<script src="{!! url('/template-admin/js/daterangepicker.js') !!}"></script>
<script src="{!! url('/template-admin/js/jquery.tagsinput.js') !!}"></script>
<script src="{!! url('/js/fancybox/jquery.fancybox.js') !!}"></script>

<script src="{!! url('/template-admin/js/custom.min.js') !!}"></script>
@yield('script')
<script src="{!! url('/template-admin/js/script.js') !!}"></script>
{{-- Các script chỉ xuất hiện ở duy nhất file này: --}}
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var selected_ids = '';//danh sách id đc chọn
    Dropzone.options.sudoUploadForm = {
        //http://www.dropzonejs.com/#configuration-options
        url:'/admin/media/store',
        paramName: "file",
        maxFilesize: 5, // MB
        parallelUploads: 1,
        acceptedFiles: "image/*,application/pdf,.doc,.docx,.xls,.xlsx,.csv,.ppt,.pptx",
        //acceptedFiles: '.jpeg,.jpg,.png,.gif,.svg',
        //autoProcessQueue: false,
        chunksUploaded: function(file, done) {
            $.ajax({
                type: 'POST',
                url: '/admin/media/store',
                dataType: "json",
                contentType: "application/json"
            });
        },
        error: function(file, response) {
            file.previewElement.classList.add("dz-error");
            $('.dz-error').find(".dz-error-mark").css({'background':'red','border-radius':'30px'});
        },
        processing: function (file) {
            //có thể thêm processbar cho đẹp
        },
        success: function(file,response) {
            file.previewElement.classList.add("dz-success");
            $('.dz-success').find(".dz-success-mark").css({'background':'green','border-radius':'30px','opacity':'1'});
            if(response.success) {
                $.each(response.result, function( key, value ) {
                    selected_ids += value.id+',';
                });
            }
        },
        queuecomplete: function() {
            console.log('All done');
            selected_ids = selected_ids.replace(/,+$/, '');//bỏ dấu , ở cuối
            window.location.href = '/admin/media/library?view=list&type={{$type}}&element={{$element}}&text={{$text}}&selected_ids='+selected_ids;
        }
    };
</script>
<script>
    //Click chọn ảnh để chèn vào nội dung
    $('#media-list').on('click','.media-item',function () {
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.media-info-img').html('');
            $('.media-info-form').css('display','none');
            $('#media-chose').prop('disabled',true);
        }else{
            if('{{$type}}' == 'single') {
                $('.media-item').removeClass('active');
            }
            $(this).addClass('active');
            var id = $(this).attr('data-id');
            var user = $(this).attr('data-user');
            var name = $(this).attr('data-name');
            var ext = $(this).attr('data-ext');
            var title = $(this).attr('data-title');
            var caption = $(this).attr('data-caption');
            var time = $(this).attr('data-time');
            var imgext = ['jpg','jpeg','png','gif','svg'];
            if(imgext.indexOf(ext) >= 0) {
                var src = $(this).find('img').attr('src');
                var html = '<img src="'+src+'"/>';
            }else {
                var html = '';
            }    
            html += '<p style="padding: 5px 0;font-size: 1.2em;"><b>{{__("Tên")}}: '+name+'</b></p>';
            html += '<p>{{__("Thêm lúc")}}: '+time+'</p>';
            $('.media-info-img').html(html);
            $('.media-info-form').find('#media-setting-id').val(id);
            $('.media-info-form').find('#media-setting-title').val(title);
            $('.media-info-form').find('#media-setting-caption').val(caption);
            $('.media-info-form').css('display','block');
            $('#media-chose').prop('disabled',false);
        }
    });
    //Tìm ảnh theo tên
    var searchImage = false;
    $('#media-search-name').on('keyup',function () {
        var keyword = $(this).val();
        if(searchImage == false){
            searchImage = true;
            $.ajax({
                url:'/admin/media/search',
                dataType:'json',
                type:'post',
                data:{keyword:keyword},
                beforeSend:function(){
                    $('#media-list-control').append('<img id="loading-tiny" src="/template-admin/images/loading.gif" style="display: inline-block"/>');
                },
                success:function(result){
                    $('#loading-tiny').remove();
                    searchImage = false;
                    if(result.status == 1) {
                        $('#media-list-wrap').html(result.html);
                    }else {
                        $('#media-list-wrap').html('');
                    }
                }
            });
        }
    });

    //Có thay đổi thông tin tiêu đề, mô tả hay không
    var updateImage = false;
    $('#media-setting-title').change(function () {
        updateImage = true;
    });
    $('#media-setting-caption').change(function () {
        updateImage = true;
    });
    //Click nút cập nhật thông tin ảnh
    $('#media-setting-btn').on('click',function (e) {
        e.preventDefault();
        if(updateImage) {
            var id = $('#media-setting-id').val();
            var title = $('#media-setting-title').val();
            var caption = $('#media-setting-caption').val();
            $.ajax({
                url:'/admin/media/update',
                dataType:'json',
                type:'post',
                data:{id:id,title:title,caption:caption},
                success:function(result){
                    var alert_type = 'warning';
                    if(result.status == 1) alert_type = 'success';
                    var alert_str = '<div class="alert alert-'+alert_type+' alert-dismissible show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+result.message+'</div>';
                    $('.media-info-form').append(alert_str);
                }
            });
        }
        return false;
    });

    //Click nút chèn ảnh
    $('#media-chose').click(function () {
        var type = '{{$type}}';
        var element = '{{$element}}';
        switch (type) {
            case 'file':
                var id = $('#media-setting-id').val();
                var title = $('#media-setting-title').val();
                var caption = $('#media-setting-caption').val();
                var src = $('.media-item.active').attr('data-src');
                var name = $('.media-item.active').attr('data-name');
                window.parent.$('#{{$element}}').parent().find('input[type=hidden]').val(src);
                window.parent.$('#{{$element}}').parent().find('.thumb-file').html(name);
                window.parent.$('.fancybox-close').click();
                break;
            case 'multi_file':
                $('.media-item.active').each(function( index ) {
                    var title = $(this).attr('data-title');
                    var caption = $(this).attr('data-caption');
                    var name = $(this).attr('data-name');
                    var src = $(this).attr('data-src');
                    var str = '';
                    str += '<div class="result_image_item" title="'+src+'"><input type="hidden" name="{{$element}}[]" value="'+src+'">'+
                        '<div class="file-item">'+name+'</div>'+
                        '<a href="javascript:;" class="del_img" onclick="return media_remove_item(this);"><i class="fa fa-times"></i></a></div>';
                    window.parent.$('#{{$element}} .result_image').append(str);
                    window.parent.$('#{{$element}} .result_image').sortable();
                });
                window.parent.$('.fancybox-close').click();
                break;
            case 'single':
                var id = $('#media-setting-id').val();
                var title = $('#media-setting-title').val();
                var caption = $('#media-setting-caption').val();
                var src = $('.media-item.active').find('img').attr('src');
                window.parent.$('#{{$element}}').parent().find('input[type=hidden]').val(src);
                window.parent.$('#{{$element}}').parent().find('img').prop('src',src);
                window.parent.$('.fancybox-close').click();
                break;
            case 'tinymce':
                var str = '';
                $('.media-item.active').each(function( index ) {
                    var id = $(this).attr('data-id');
                    var title = $(this).attr('data-title');
                    var caption = $(this).attr('data-caption');
                    var src = $(this).find('img').attr('src');
                    str += '<figure class="sudo-media-item" data-id="'+id+'">';
                    str += '<img src="'+src+'" alt="'+title+'">';
                    if(caption != '') {
                        str += '<figcaption>'+caption+'</figcaption>';
                    }
                    str += '</figure>';
                });
                window.parent.tinyMCE.get('{{$element}}').execCommand("mceInsertContent",false,str);
                window.parent.$('.fancybox-close').click();
                break;
            case 'slide':
                $('.media-item.active').each(function( index ) {
                    var title = $(this).attr('data-title');
                    var caption = $(this).attr('data-caption');
                    var src = $(this).find('img').attr('src');
                    var str = '';
                    str += '<div class="result_image_item"><input type="hidden" name="{{$element}}[]" value="'+src+'">'+
                        '<img src="'+src+'" alt="Không có ảnh">'+
                        '<a href="javascript:;" class="del_img" onclick="return media_remove_item(this);"><i class="fa fa-times"></i></a></div>';
                    window.parent.$('#{{$element}} .result_image').append(str);
                    window.parent.$('#{{$element}} .result_image').sortable();
                });
                window.parent.$('.fancybox-close').click();
                break;
        }
    });
</script>
@if($selected_ids != '0')
    @php
        $selected_array = explode(',',$selected_ids);
    @endphp
    <script>
        //nếu có selected thì click vào media-item để load thông tin vào form cập nhật thông tin
        $(document).ready(function() {
            var selected = $('#media-item-{{$selected_array[0]}}');
            selected.addClass('active');
            var id = selected.attr('data-id');
            var user = selected.attr('data-user');
            var name = selected.attr('data-name');
            var ext = $(this).attr('data-ext');
            var title = selected.attr('data-title');
            var caption = selected.attr('data-caption');
            var time = selected.attr('data-time');
            var imgext = ['jpg','jpeg','png','gif','svg'];
            if(imgext.indexOf(ext) >= 0) {
                var src = $(this).find('img').attr('src');
                var html = '<img src="'+src+'"/>';
            }else {
                var html = '';
            }
            html += '<p style="padding: 5px 0;font-size: 1.2em;"><b>{{__("Tên")}}: '+name+'</b></p>';
            html += '<p>{{__("Thêm lúc")}}: '+time+'</p>';
            $('.media-info-img').html(html);
            $('.media-info-form').find('#media-setting-id').val(id);
            $('.media-info-form').find('#media-setting-title').val(title);
            $('.media-info-form').find('#media-setting-caption').val(caption);
            $('.media-info-form').css('display','block');
            $('#media-chose').prop('disabled',false);
        });
    </script>
@endif
</body>
</html>