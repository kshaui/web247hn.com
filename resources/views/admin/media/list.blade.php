@php
    $selected_array = explode(',',$selected_ids);
    if(config('app.storage_type') == 'objectstorage') {
        $media_domain = config('filesystems.disks.sop.container_url');
    }elseif(config('app.storage_type') == 'digitalocean') {
        $media_domain = config('filesystems.disks.do.domain').'/'.config('filesystems.disks.do.folder');
    }else {
        $media_domain = url('uploads');
    }
@endphp
@foreach($data as $media)
    @if(in_array($media->ext,$image_extension))
    <div id="media-item-{{$media->id}}" class="media-item @if(in_array($media->id,$selected_array)) {{'active'}} @endif"
         data-id="{{$media->id}}"
         data-user="{{$media->user_id}}"
         data-name="{{$media->name}}"
         data-ext="{{$media->ext}}"
         data-title="{{$media->title}}"
         data-caption="{{$media->caption}}"
         data-time="{{date('H:i:s d-m-Y',strtotime($media->created_at))}}"
         data-src="{{$media_domain}}/{{date('Y',strtotime($media->created_at))}}/{{date('m',strtotime($media->created_at))}}/{{$media->name}}">
        <img src="{{$media_domain}}/{{date('Y',strtotime($media->created_at))}}/{{date('m',strtotime($media->created_at))}}/{{$media->name}}" />
    </div>
    @else
    <div id="media-item-{{$media->id}}" class="media-item @if(in_array($media->id,$selected_array)) {{'active'}} @endif"
         data-id="{{$media->id}}"
         data-user="{{$media->user_id}}"
         data-name="{{$media->name}}"
         data-ext="{{$media->ext}}"
         data-title="{{$media->title}}"
         data-caption="{{$media->caption}}"
         data-time="{{date('H:i:s d-m-Y',strtotime($media->created_at))}}"
         data-src="{{$media_domain}}/{{date('Y',strtotime($media->created_at))}}/{{date('m',strtotime($media->created_at))}}/{{$media->name}}">
        <div class="file-not-image">{{$media->name}}</div>
        <div class="ext">{{$media->ext}}</div>
    </div>
    @endif
@endforeach