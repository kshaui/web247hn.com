@extends('admin.layouts.app')
@section('title')
<h3>Thay đổi mật khẩu quản trị viên</h3>
@endsection()
@section('title2')
<h2>Những trường đánh dấu (<span style="color:red;">*</span>) là bắt buộc nhập.</h2>
@endsection()
@section('content')
@include('errors.error')
<div class="col-lg-12">
    @if(Session::has('flash_message'))
        <div class="alert alert-{!! Session::get('flash_level') !!}">
            {!! Session::get('flash_message') !!}
        </div>
    @endif
</div>
<style>
	.form-actions{
		margin-left: 206px;
	}
</style>
<form action="" method="post" class="form-horizontal" accept-charset="utf-8">
	@include('admin.layouts.form')
</form>
@endsection()