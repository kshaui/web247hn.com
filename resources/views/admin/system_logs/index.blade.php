<td>
    {{$array_admins[$value->admin_id]}}
</td>
<td>
    {{$value->ip}}
</td>
<td>
    {{date('H:i:s d/m/Y',strtotime($value->time))}}
</td>
<td>
    {{$value->title}}
</td>
<td>
    {!! isset(config('app.log_type')[$value->type]) ? config('app.log_type')[$value->type] : $value->type !!}
</td>
<td>
    {!! isset(config('app.log_table')[$value->table]) ? config('app.log_table')[$value->table] : $value->table !!}
</td>
<td>
	@if($value->type != 'login')
    	<a href="/admin/system_logs/{{$value->id}}" target="_blank">Chi tiết</a>
    @endif
</td>