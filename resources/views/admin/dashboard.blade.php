@extends('admin.layouts.app')

@section('title')
  <h3>{{__('Chào mừng bạn đến với trang quản trị')}}</h3>
@endsection

@section('title2')
    <h4>{{__('Hệ thống quản trị')}}</h4>
@endsection

@section('content')
    <h4>{{__('Hướng dẫn quản trị')}}</h4>
    <ul>
        <li>{{__('Quản lý các Modules theo các danh mục Modules bên trái')}}</li>
        <li>{{__('Nội dung các Modules được hiển thị ở khung bên phải')}}</li>
        <li>{{__('Có thể mở rộng màn hình module bằng cách mở rộng/thu nhỏ thanh điều hướng ở header')}}</li>
        <li>{{__('Icon user : Truy cập Module quản lý thông tin cá nhân - Thoát phiên đăng nhập')}}</li>
    </ul>
@endsection
