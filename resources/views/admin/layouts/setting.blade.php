@extends('admin.layouts.app')
@section('title')
    <h3>{{ $title }}</h3>
@endsection()
@section('content')
    @include('errors.error')

    @php
    $array_valid = [];
    foreach($data_form as $value) {
        if(isset($value['required']) && $value['required'] == 1) {
            $array_valid[] = $value['name'];
        }
    }
    $string_valid = '';
    if(count($array_valid) > 0) {
        $string_valid = 'onsubmit="validForm(this,\''.implode(',',$array_valid).'\');return false;"';
    }
    @endphp

    <form action="" class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" {!! $string_valid !!}>
        <input type="hidden" name="locale" value="{!! App::getLocale() !!}">
        @include('admin.layouts.form')
    </form>

@endsection()
@section('css')
<link rel="stylesheet" href="/template-admin/css/menu.css?v=1234">
@endsection
@section('script')
    <script src="/template-admin/js/jquery.nestable.js"></script>
    <script>
        $(document).ready(function(){
            $('body').on('click','.remove-menu',function () {
                $(this).closest('li').remove();
            });
            $('body').on('click','.edit-now',function () {
                $(this).closest('li.dd-item').find('.edit-menu').slideToggle(200);
            });
            $('body').on('click','.update-now',function () {
                var btn_group = $(this).closest('li.dd-item');
                var link = $(btn_group).find('.link-menu-edit').val();
                var data_name =$(btn_group).find('.name-menu-edit').val();
                if ($(btn_group).find('.taget-menu-edit').is(":checked")){
                    var taget = 1;
                }else{
                    var taget = 0;
                }
                if ($(btn_group).find('.rel-menu-edit').is(":checked")){
                    var rel = 1;
                }else{
                    var rel = 0;
                }

                if(data_name == ''){
                    alert('Tên menu không được để trống');
                }else{
                    $(btn_group).find('.add img.img2').css('display','inline');
                    setTimeout(function(){
                        $('.add img.img2').css('display','none');
                        $(btn_group).attr('data-name',data_name);
                        $(btn_group).attr('data-link',link);
                        $(btn_group).attr('data-taget',taget);
                $(btn_group).attr('data-rel',rel);
                        $(btn_group).find('.dd-handle').html(data_name);
                        $(btn_group).find('.edit-menu').fadeOut();
                    },500);
                }
            });
        });
        function toggle_menu(classs, name){
            $('#'+name+' .add-menu-'+classs).slideToggle(200);
        }
        function add_menu(id){
            var link = $('#'+id+' .link-menu').val();
            var name = $('#'+id+' .name-menu').val();
            if ($('#'+id+' .taget-menu').is(":checked")){
                var taget = 1;
            }else{
                var taget = 0;
            }
            if ($('#'+id+' .rel-menu').is(":checked")){
                var rel = 1;
            }else{
                var rel = 0;
            }

            if(name == ''){
                alert('Tên menu không được để trống')
            }else{
                var html = '<li class="dd-item" data-name = "'+name+'" data-link = "'+link+'" data-table="" data-id="0" data-taget="'+taget+'" data-rel="'+rel+'"><div class="dd-handle">'+name+'</div><p class="p-action"><a class="a1 edit-now" href="javascript:;">Sửa</a><a class="a2 remove-menu" href="javascript:;">Xóa</a></p>';
                    html += '<div class="add-menu edit-menu-custom edit-menu">'+
                        '<h5>Sửa menu</h5>'+
                        '<div class="form-group">'+
                        '<label>Tên menu</label>'+
                        '<input type="text" class="name-menu-edit" value="'+name+'">'+
                        '</div>'+
                        '<div class="form-group">'+
                        '<label>Link</label>'+
                        '<input type="text" class="link-menu-edit" value="'+link+'">'+
                        '</div>'+
                        '<div class="form-group">'+
                        '<label>Mở tab mới</label>';
                        if(taget == 1){
                            html += '<input style="height: 15px;width: 20px;" checked type="checkbox" class="taget-menu-edit">';
                        }else{
                            html += '<input style="height: 15px;width: 20px;" type="checkbox" class="taget-menu-edit">';
                        }
                        html += '</div>'+
                        '<div class="form-group">'+
                        '<label>Nofollow</label>';
                        if(rel == 1){
                            html += '<input style="height: 15px;width: 20px;" checked type="checkbox" class="rel-menu-edit">';
                        }else{
                            html += '<input style="height: 15px;width: 20px;" type="checkbox" class="rel-menu-edit">';
                        }
                        html += '</div>'+
                        '<p class="add">'+
                        '<img class="img2" src="/template-admin/images/spinner.gif" alt="">'+
                        '<a href="javascript:;" class="update-now">Cập nhật</a>'+
                        '</p>'+
                        '</div>'+
                        '</li>';
                $('#'+id+' .add img.img1').css('display','inline');
                setTimeout(function(){
                    $('#'+id+' .add img.img1').css('display','none');
                    $('#'+id+' .link-menu').val('');
                    $('#'+id+' .name-menu').val('');
                    $('#'+id+' #ol-first').append(html);
                    $('#'+id+' .link-menu').val('');
                    $('#'+id+' .name-menu').val('');
                    $('#'+id+' .taget-menu').prop('checked', false);
                    $('#'+id+' .rel-menu').prop('checked', false);
                },500);
            }
        }
        function add_menu1(id,table){
            var name =$('#'+id+' .name-menu-'+table).val();
            var id_table =$('#'+id+' .link-menu-'+table).val();
            var link =$('#'+id+' .link-menu-'+table).find(':selected').attr('data-slug');
            if ($('#'+id+' .taget-menu-'+table).is(":checked")){
                var taget = 1;
            }else{
                var taget = 0;
            }
            if ($('#'+id+' .rel-menu-'+table).is(":checked")){
                var rel = 1;
            }else{
                var rel = 0;
            }
            if(name == ''){
                alert('Tên menu không được để trống')
            }else{
                var html = '<li class="dd-item" data-name = "'+name+'" data-link = "'+link+'" data-table="'+table+'" data-id="'+id_table+'" data-taget="'+taget+'" data-rel="'+rel+'"><div class="dd-handle">'+name+'</div><p class="p-action"><a class="a2 remove-menu" href="javascript:;">Xóa</a></p></li>';
                $('#'+id+' .add img.img1').css('display','inline');
                setTimeout(function(){
                    $('#'+id+' .add img.img1').css('display','none');
                    $('#'+id+' .link-menu').val('');
                    $('#'+id+' .name-menu').val('');
                    $('#'+id+' #ol-first').append(html);
                    $('#'+id+' .name-menu-'+table).val('');
                    $('#'+id+' .taget-menu-'+table).prop('checked', false);
                    $('#'+id+' .rel-menu-'+table).prop('checked', false);
                },500);
            }
        }
        function add_menu2(id){
            var name =$('#'+id+' .add-menu-fix-link .name-menu').val();
            var link =$('#'+id+' .add-menu-fix-link .link-menu').find(':selected').attr('data-slug');
            if ($('#'+id+' .add-menu-fix-link .taget-menu').is(":checked")){
                var taget = 1;
            }else{
                var taget = 0;
            }
            if ($('#'+id+' .add-menu-fix-link .rel-menu').is(":checked")){
                var rel = 1;
            }else{
                var rel = 0;
            }
            if(name == ''){
                alert('Tên menu không được để trống')
            }else{
                var html = '<li class="dd-item" data-name = "'+name+'" data-link = "'+link+'" data-table="fix_link" data-id="0" data-taget="'+taget+'" data-rel="'+rel+'"><div class="dd-handle">'+name+'</div><p class="p-action"><a class="a2 remove-menu" href="javascript:;">Xóa</a></p></li>';
                $('#'+id+' .add img.img1').css('display','inline');
                setTimeout(function(){
                    $('#'+id+' .add img.img1').css('display','none');
                    $('#'+id+' .link-menu').val('');
                    $('#'+id+' .name-menu').val('');
                    $('#'+id+' #ol-first').append(html);
                    $('#'+id+' .name-menu-'+table).val('');
                    $('#'+id+' .taget-menu-'+table).prop('checked', false);
                    $('#'+id+' .rel-menu-'+table).prop('checked', false);
                },500);
            }
        }
        function show_edit_menu(name, id){
            $('li#li-'+id).find('.edit-menu-'+id).slideToggle(200);
        }

        function show_edit_menu1(name,id){
            $('li#li-'+id).find('.edit-menu-'+id).slideToggle(200);
        }
        function update_menu(id){
            var link = $('#li-'+id+' .link-menu-edit').val();
            var data_name =$('#li-'+id+' .name-menu-edit').val();

            if ($('#li-'+id+' .edit-menu-'+id+' .taget-menu-edit').is(":checked")){
                var taget = 1;
            }else{
                var taget = 0;
            }
            if ($('#li-'+id+' .edit-menu-'+id+' .rel-menu-edit').is(":checked")){
                var rel = 1;
            }else{
                var rel = 0;
            }

            if(data_name == ''){
                alert('Tên menu không được để trống')
            }else{
                $('#li-'+id+' .edit-menu-'+id+' .add img.img2').css('display','inline');
                setTimeout(function(){
                    $('.add img.img2').css('display','none');
                    $('#li-'+id).attr('data-name',data_name);
                    $('#li-'+id).attr('data-link',link);
                    $('#li-'+id).attr('data-taget',taget);
                    $('#li-'+id).attr('data-rel',rel);
                    $('#li-'+id+' .handle-'+id).html(data_name);
                    $('#li-'+id+' .edit-menu-'+id).fadeOut();
                },500);
            }
        }
        function update_menu1(id){
            var data_name =$('#li-'+id+' .name-menu-edit').val();
            var id_table =$('#li-'+id+' .link-menu-edit').val();
            var link =$('#li-'+id+' .link-menu-edit').find(':selected').attr('data-slug');
            if ($('#li-'+id+' .edit-menu-'+id+' .taget-menu-edit').is(":checked")){
                var taget = 1;
            }else{
                var taget = 0;
            }
            if ($('#li-'+id+' .edit-menu-'+id+' .rel-menu-edit').is(":checked")){
                var rel = 1;
            }else{
                var rel = 0;
            }

            if(data_name == ''){
                alert('Tên menu không được để trống')
            }else{
                $('#li-'+id+' .edit-menu-'+id+' .add img.img2').css('display','inline');
                setTimeout(function(){
                    $('.add img.img2').css('display','none');
                    $('#li-'+id).attr('data-name',data_name);
                    $('#li-'+id).attr('data-link',link);
                    $('#li-'+id).attr('data-id',id_table);
                    $('#li-'+id).attr('data-taget',taget);
                    $('#li-'+id).attr('data-rel',rel);
                    $('#li-'+id+' .handle-'+id).html(data_name);
                    $('#li-'+id+' .edit-menu-'+id).fadeOut();
                },500);
            }
        }
        function remove_menu(name,id){
            $('#'+name+' #li-'+id).remove();
        }
    </script>
@endsection