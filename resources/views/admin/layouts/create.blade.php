@extends('admin.layouts.app')
@section('title')
    <h3>{{__('Thêm mới '.$module_name.'')}}</h3>
@endsection()
@section('title2')
    <h2>{!! __('Những trường đánh dấu :value là bắt buộc nhập',['value' => '(<span style="color:red;">*</span>)']) !!}</h2>
@endsection()
@section('content')
    @include('errors.error')

    @php
    $array_valid = [];
    foreach($data_form as $value) {
        if(isset($value['required']) && $value['required'] == 1) {
            $array_valid[] = $value['name'];
        }
    }
    $string_valid = '';
    if(count($array_valid) > 0) {
        $string_valid = 'onsubmit="validForm(this,\''.implode(',',$array_valid).'\');return false;"';
    }
    
    $from_record = app('request')->input('from_record',0);
    $new_language = app('request')->input('new_language',App::getLocale());
    if($has_locale && $new_language != App::getLocale()) {
        @endphp
        <form action="{{ route('switchLanguage') }}" id="form-switchLanguage" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="locale" value="{{$new_language}}">
        </form>
        <script>document.getElementById('form-switchLanguage').submit();</script>
        @php
        exit();
    }
    if($from_record != 0) {
        $source_record = DB::table($table_name)->where('id', $from_record)->first();
        if(!$source_record) {
            echo __('Không tìm thấy bản ghi');
            die();
        }else{
            if($new_language == $source_record->locale) {
                @endphp
                <script>
                 window.location.href = '{{route($table_name.".edit", $source_record->id)}}';
                </script>
                @php
                exit();
            }
        }
    }
    @endphp

    <form action="{!! route($table_name.'.store') !!}" class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" {!! $string_valid !!}>
        
        @if($has_locale)
        <input type="hidden" name="locale" value="{{$new_language}}">
        <input type="hidden" name="from_record" value="{{$from_record}}">
        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">{{__('Loại')}}</label>
            <div class="controls col-md-9 col-sm-10 col-xs-12">
                <label class="control-label">
                @if(isset($source_record) && $source_record) 
                {{__('Thêm dữ liệu tương ứng từ bản gốc')}}: <b><a href="{{route($table_name.'.edit', $source_record->id)}}">{{$source_record->name}}</a></b>
                @else 
                {{__('Thêm mới')}}
                @endif
                </label>
            </div>
        </div>
        @endif

        @include('admin.layouts.form')
        
        @if($has_seo)
            @php
            $meta_seo_type = $table_name;
            $meta_seo_type_id = 0;
            @endphp
            @include('admin.layouts.metaseo')
        @endif
    </form>

@endsection()
@section('script')
    <script>
        $(document).ready(function(){
            $('.slug-title').on("change",function(){
                var field = $(this).attr('data-slug');
                var title = $(this).val();
                var token = $("input[name='_token']").val();
                $.ajax({
                    type:'post',
                    url:'/admin/ajax/get-slug',
                    data:{'_token':token,title:title},
                    success:function(html){
                        $('#'+field).val(html);
                    }
                })
            });
        });
    </script>
@endsection()

