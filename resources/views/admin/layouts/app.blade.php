<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Administrator Managerment</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{!! url('/template-admin/css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! url('/template-admin/css/font-awesome.min.css') !!}" rel="stylesheet">
    <link href="{!! url('/template-admin/css/jquery.datetimepicker.min.css') !!}" rel="stylesheet">
    <link href="{!! url('/template-admin/css/daterangepicker.css') !!}" rel="stylesheet">
    <link href="{!! url('/js/fancybox/jquery.fancybox.css') !!}" rel="stylesheet">
    <link href="{!! url('/template-admin/css/custom.min.css') !!}" rel="stylesheet">
    <link href="{!! url('/template-admin/css/style1.css') !!}" rel="stylesheet">
    @yield('css')
    <script src="{!! url('/template-admin/js/jquery.min.js') !!}"></script>
</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="/" class="site_title" target="_blank"><i class="fa fa-desktop"></i>
                        <span>{{config('app.name')}}</span></a>
                </div>
                <div class="clearfix"></div>
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">
                            <li><a href="{!! url('admin') !!}"><i class="fa fa-tachometer "></i>{{__('Bảng điều khiển')}}</a>
                            </li>
                            @foreach (config('modules.module') as $key=>$item)
                                @switch($key)
                                    @case('settings')
                                        @if(checkRole($key.'_access'))
                                            <li>
                                                <a><i class="fa {!! config('modules.icon')[$key] !!}" aria-hidden="true"></i>{{__('Cấu hình')}} <span class="fa fa-chevron-down"></span></a>
                                                <ul class="nav child_menu">
                                                    @foreach($item as $k => $val)
                                                        @if(checkRole($key.'_'.$val) && $val !="access")
                                                            <li><a href="/admin/{!! $key !!}/{!! $val !!}">{{__(config('modules.name')[$val])}}</a></li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endif
                                        @break

                                    @case('products_categories') @break{{-- cho danh mục sản phẩm vào menu sản phẩm nên bỏ qua nó --}}
                                    @case('news_categories') @break{{-- cho danh mục tin tức vào menu tin tức nên bỏ qua nó --}}

                                    @case('products')
                                    @if(checkRole($key.'_access'))
                                        <li><a><i class="fa {!! config('modules.icon')[$key] !!}"></i>{{__(config('modules.name')[$key])}} <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                @if(checkRole($key.'_create'))
                                                    <li><a href="{!! route($key.'.create') !!}">{{__('Thêm mới')}}</a></li>
                                                @endif
                                                <li><a href="{!! route($key.'.index') !!}">{{__('Danh sách')}}</a></li>
                                                @if(checkRole('products_categories_access'))
                                                    <li><a href="{!! route('products_categories.index') !!}">{{__('Danh mục')}}</a></li>
                                                @endif
                                            </ul>
                                        </li>
                                    @endif
                                    @break

                                    @case('news')
                                    @if(checkRole($key.'_access'))
                                        <li><a><i class="fa {!! config('modules.icon')[$key] !!}"></i>{{__(config('modules.name')[$key])}} <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                @if(checkRole($key.'_create'))
                                                    <li><a href="{!! route($key.'.create') !!}">{{__('Thêm mới')}}</a></li>
                                                @endif
                                                <li><a href="{!! route($key.'.index') !!}">{{__('Danh sách')}}</a></li>
                                                @if(checkRole('products_categories_access'))
                                                    <li><a href="{!! route('news_categories.index') !!}">{{__('Danh mục')}}</a></li>
                                                @endif
                                            </ul>
                                        </li>
                                    @endif
                                    @break

                                    @break

                                    @case('module-dac-biet-cua-ban')

                                    @break

                                    @case('module-dac-biet-cua-ban')

                                        @break
                                    @case('system_logs')
                                        @if(checkRole($key.'_access'))
                                            <li>
                                                <a href="{!! route($key.'.index') !!}"><i class="fa {!! config('modules.icon')[$key] !!}"></i>{{__(config('modules.name')[$key])}}</a>
                                            </li>
                                        @endif
                                    @break
                                    @default
                                        @if(checkRole($key.'_access'))
                                            <li><a><i class="fa {!! config('modules.icon')[$key] !!}"></i>{{__(config('modules.name')[$key])}} <span class="fa fa-chevron-down"></span></a>
                                                <ul class="nav child_menu">
                                                    @if(checkRole($key.'_create'))
                                                        <li><a href="{!! route($key.'.create') !!}">{{__('Thêm mới')}}</a></li>
                                                    @endif
                                                    <li><a href="{!! route($key.'.index') !!}">{{__('Danh sách')}}</a></li>
                                                </ul>
                                            </li>
                                        @endif
                                @endswitch
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->
            </div>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <div class="nav navbar-nav navbar-left">
                        {{-- <a id="purge-cache" href="javascript:;"><i class="fa fa-bug"></i> Purge cache</a> --}}
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               style="font-size: 16px;" aria-expanded="false">
                                <img src="{!! url('/template-admin/images/no-avatar.png') !!}"
                                     alt="{!! Auth::guard('admin')->user()->name !!}">
                                {!! Auth::guard('admin')->user()->name !!}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="{!! route('admin.admin_user.changePassword') !!}">{{__('Đổi mật khẩu')}}</a></li>
                                <li><a href="{!! route('admin.logout') !!}"><i class="fa fa-sign-out pull-right"></i>{{__('Đăng xuất')}}</a></li>
                            </ul>
                        </li>
                        <form action="{{ route('switchLanguage') }}" class="form-lang" method="post">
                            <p style="display: inline;padding-right: 10px;">{!!__('Ngôn ngữ')!!} ({{App::getLocale()}})</p>
                            <select name="locale" class="language" onchange='this.form.submit();'>
                                @foreach(config('app.languages') as $key=>$value)
                                    <option value="{{$key}}" {{ App::getLocale() == $key ? 'selected' : '' }}>{!! __($value) !!}</option>
                                @endforeach
                            </select>
                            {{ csrf_field() }}
                        </form>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        @yield('title')
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                @yield('title2')
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>
<script src="{!! url('/js/tinymce/tinymce.min.js') !!}"></script>
<script src="{!! url('/template-admin/js/moment.min.js') !!}"></script>
<script src="{!! url('/template-admin/js/bootstrap.min.js') !!}"></script>
<script src="{!! url('/template-admin/js/bootbox.min.js') !!}"></script>
<script src="{!! url('/template-admin/js/jquery.datetimepicker.full.min.js') !!}"></script>
<script src="{!! url('/template-admin/js/daterangepicker.js') !!}"></script>
<script src="{!! url('/template-admin/js/jquery.tagsinput.js') !!}"></script>
<script src="{!! url('/template-admin/js/jquery.sortable.min.js') !!}"></script>
<script src="{!! url('/js/fancybox/jquery.fancybox.js') !!}"></script>
<script src="{!! url('/template-admin/js/custom.min.js') !!}"></script>
<script src="{!! url('/template-admin/js/media.js') !!}"></script>
<script src="{!! url('/template-admin/js/script.js') !!}"></script>
@yield('script')
</body>
</html>
