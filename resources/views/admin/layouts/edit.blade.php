@extends('admin.layouts.app')
@section('title')
    <h3>{{__('Sửa '.$module_name.'')}}</h3>
@endsection()
@section('title2')
    <h2>{!! __('Những trường đánh dấu :value là bắt buộc nhập',['value' => '(<span style="color:red;">*</span>)']) !!}</h2>
@endsection()
@section('content')
    @include('errors.alert')
    @include('errors.error')

    @php
    $array_valid = [];
    foreach($data_form as $value) {
        if(isset($value['required']) && $value['required'] == 1) {
            $array_valid[] = $value['name'];
        }
    }
    $string_valid = '';
    if(count($array_valid) > 0) {
        $string_valid = 'onsubmit="validForm(this,\''.implode(',',$array_valid).'\');return false;"';
    }
    
    if($has_locale){
        $current_record = DB::table($table_name)->where('id', $id)->first();
        $source_record = getLocaleRecordSource($table_name,$id);
        if($has_locale && $current_record->locale != App::getLocale()) {
            @endphp
            <form action="{{ route('switchLanguage') }}" id="form-switchLanguage" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="locale" value="{{$current_record->locale}}">
            </form>
            <script>document.getElementById('form-switchLanguage').submit();</script>
            @php
            exit();
        }
    }
    @endphp

    <form action="{!! route($table_name.'.update',$id) !!}" class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" {!! $string_valid !!}>
        {{ method_field('PUT') }}

        @if($has_locale)
        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">{{__('Ngôn ngữ')}}</label>
            <div class="controls col-md-2 col-sm-6 col-xs-12">
                <p style="font-size: 18px;">{{__(config('app.languages')[$current_record->locale])}}</p>
            </div>
            @php
            foreach(config('app.languages') as $key=>$value){
                if($key != $current_record->locale) {
                    $locale_record = getLocaleRecord($table_name,$id,$key);
                    @endphp
                    <div class="controls col-md-4 col-sm-6 col-xs-12">
                        @if($locale_record)
                            <a href="{{route($table_name.'.edit', $locale_record->id)}}?set_locale={{$key}}">{{__('Sửa')}} <img src="/flags/{{$key}}.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        @else 
                            <a href="{{route($table_name.'.create')}}?from_record={{$source_record->id}}&new_language={{$key}}&set_locale={{$key}}">{{__('Thêm')}} <img src="/flags/{{$key}}.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        @endif
                    </div>
                    @php
                }
            }
            @endphp
        </div>
        @endif
        
        @include('admin.layouts.form')
        
        @if($has_seo)
            @php
            $meta_seo_type = $table_name;
            $meta_seo_type_id = $id;
            @endphp
            @include('admin.layouts.metaseo')
        @endif
    </form>

@endsection()

@section('script')
@endsection()