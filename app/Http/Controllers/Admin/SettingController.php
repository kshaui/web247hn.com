<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use App;

/**
 * Class SettingController
 * @package App\Http\Controllers\Admin
 *
 * Phần cấu hình của core chúng ta chia thành 2 phần chính để tiện cho tối ưu query ngoài frontend:
 * - Cấu hình trang chủ: Seo (title, description, ...) của trang chủ, các nội dung chỉ hiện thị ở trang chủ như banner, các nội dung cố định ...
 * - Cấu hình chung: cho các nội dung chung trên toàn bộ các trang như: html head, hotline, link ở header, các thông tin ở footer ...
 *
 */
class SettingController extends Controller
{
    private function postData($setting_name, $data){
        $locale = $data['locale'] ?? App::getLocale();
        unset($data['_token']);
        unset($data['submit']);
        unset($data['locale']);
        $data = base64_encode(json_encode($data));
        if(DB::table('options')->where('name',$setting_name)->where('locale', $locale)->exists()){
            DB::table('options')->where('name',$setting_name)->where('locale', $locale)->update(['value'=>$data]);
        }else{
            DB::table('options')->insert(['locale'=>$locale,'name'=>$setting_name,'value'=>$data]);
        }
    }

    public function general(Request $request) {
        $setting_name = 'general';
        $title = "Cấu hình chung";

        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();

        // post data
        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }
        //
        $option = DB::table('options')->select('value')->where('name',$setting_name)->where('locale', App::getLocale())->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $form = new MyForm();
        
        $data_form[] = $form->checkbox('robots',isset($data['robots']) ? $data['robots'] : '',1,' Cho phép google lập chỉ mục nội dung Website');//Cấu hình quan trọng
        $data_form[] = $form->textarea('html_head',isset($data['html_head']) ? $data['html_head'] : '',0,'Các thẻ html chèn vào head','');
        $data_form[] = $form->text('email_admin',isset($data['email_admin']) ? $data['email_admin'] : '',0,'Email nhận thông báo khi có đơn hàng','');

        $data_form[] = $form->customMenu('menu_primary', isset($data['menu_primary']) ? $data['menu_primary'] : '','Cấu hình menu header');
        $data_form[] = $form->customMenu('menu_footer', isset($data['menu_footer']) ? $data['menu_footer'] : '','Cấu hình menu footer');

        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title'));
    }

    public function home(Request $request) {
        $setting_name = 'home';
        $title = "Cấu hình trang chủ";

        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();

        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }

        $option = DB::table('options')->select('value')->where('name',$setting_name)->where('locale', App::getLocale())->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $form = new MyForm();
        $data_form[] = $form->title('Cấu hình SEO trang chủ');
        $data_form[] = $form->text('meta_title',isset($data['meta_title']) ? $data['meta_title'] : '',0,'Meta title');
        $data_form[] = $form->textarea('meta_description',isset($data['meta_description']) ? $data['meta_description'] : '',0,'Meta description','');
        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title'));
    }

    public function seo(Request $request) {
        $setting_name = 'seo';
        $title = "Cấu hình Seo";

        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();

        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }

        $option = DB::table('options')->select('value')->where('name',$setting_name)->where('locale', App::getLocale())->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $form = new MyForm();
        $data_form[] = $form->title('Cấu hình SEO trang liên hệ');
        $data_form[] = $form->text('meta_title_contact',isset($data['meta_title_contact']) ? $data['meta_title_contact'] : '',0,'Meta title');
        $data_form[] = $form->textarea('meta_description_contact',isset($data['meta_description_contact']) ? $data['meta_description_contact'] : '',0,'Meta description','');
        $data_form[] = $form->title('Cấu hình SEO danh mục tin tức chung');
        $data_form[] = $form->text('meta_title_news',isset($data['meta_title_news']) ? $data['meta_title_news'] : '',0,'Meta title');
        $data_form[] = $form->textarea('meta_description_news',isset($data['meta_description_news']) ? $data['meta_description_news'] : '',0,'Meta description','');
        $data_form[] = $form->title('Cấu hình SEO danh mục sản phẩm chung');
        $data_form[] = $form->text('meta_title_products',isset($data['meta_title_products']) ? $data['meta_title_products'] : '',0,'Meta title');
        $data_form[] = $form->textarea('meta_description_products',isset($data['meta_description_products']) ? $data['meta_description_products'] : '',0,'Meta description','');
        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title'));
    }
}
