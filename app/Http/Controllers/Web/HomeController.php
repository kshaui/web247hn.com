<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\MyClass\Categories;
use App;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $setting_home = $this->getOption('home');
        dump($setting_home);

        foreach (config('app.languages') as $key => $value) {
            $switch_languages[$key] = route('web.home.'.$key);
        }

        $meta_seo = $this->meta_seo('',0,[
            'title' => $setting_home['meta_title'] ?? 'Trang chủ',
            'description'=> $setting_home['meta_description'] ?? 'Mô tả trang chủ',
            'image' => url('/').'/assets/img/logo.png'
        ],$switch_languages);

        $admin_bar_edit = route('admin.setting.home');
        return view('web.home',compact('meta_seo','admin_bar_edit','switch_languages'));
    }
}
