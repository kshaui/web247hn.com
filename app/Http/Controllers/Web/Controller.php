<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;
use App;
use DB;
use View;
use App\ProductsCategory;

class Controller extends BaseController
{
    public $locale;


    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->setLanguage($request);

            $data_menu = $this->getOption('menu',$this->locale);

            //Demo share danh mục sp theo locale hiện tại ở constructor
            $products_categories = ProductsCategory::where('status',1)->where('locale',$this->locale)->get();
            View::share('products_categories', $products_categories);

            return $next($request);
        });

        //Biến để chuyển ngôn ngữ cho url theo bản ghi tương ứng, biến này sẽ được ghi đè ở các action controller chi tiết
        //Biến này dùng cho select chuyển ngôn ngữ và các thẻ hreflang ở layout meta_seo
        foreach (config('app.languages') as $key => $value) {
            if($key == config('app.fallback_locale')) {
                $switch_languages[$key] = '/';
            }else {
                $switch_languages[$key] = '/'.$key;
            }
        }
        View::share('switch_languages',$switch_languages);
    }

    /**
     * @param string $type - tên kiểu trong bảng meta_seo
     * @param int $id - id trong bảng meta_seo
     * @param array $options - các trường mặc định hoặc không có trong bảng meta_seo: title | description | robots | type | url | image |
     */
    public function meta_seo($type='',$id=0,$options,$switch_languages = []) {
        $meta_seo['switch_languages'] = $switch_languages;
        if(count($options)) {
            foreach ($options as $key=>$value) {
                $meta_seo[$key] = $value;
            }
        }
        if ($type != '' && $id != 0) {
            $data_seo = DB::table('meta_seo')->where('type',$type)->where('type_id',$id)->first();
            if($data_seo) {
                if($data_seo->title!=''){
                    $meta_seo['title'] = $data_seo->title;
                }
                if($data_seo->description!=''){
                    $meta_seo['description'] = $data_seo->description;
                }
                $meta_seo['robots'] = $data_seo->robots;
            }
        }
        return $meta_seo;
    }

    public function getOption($name,$locale = null) {
        if (null === $locale) {
            $locale = App::getLocale();
        }
        
        $option = DB::table('options')->select('value')->where('name',$name)->where('locale',$locale)->first();
        if(empty($option)){
            return null;
        }else{
            return json_decode(base64_decode($option->value),true);
        }
    }

    /**
     * set locale theo route
     * @param [type] $request [description]
     */
    public function setLanguage($request){
        //Cắt lấy ký tự sau dấu chấm cuối cùng trong tên route => chính là locale
        $route_name = $request->route()->getName();
        $route_locale = substr($route_name, strrpos($route_name, '.')+1);

        //Kiểm tra lại xem có phải key trong config cho chắc
        if (array_key_exists($route_locale, config('app.languages'))) {
            $locale = $route_locale;
        }else {
            $locale = config('app.fallback_locale');
        }

        //Set locale nếu không trùng với locale hiện tại
        if($locale != App::getLocale()) {
            App::setLocale($locale);
            session(['locale' => $locale]);
        }

        $this->locale = $locale;
        View::share('locale', $locale);
    }
}
