<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mail;
use App\Mail\TestEmail;

class TestController extends Controller
{

    public function index()
    {
        //sending a test email
        Mail::to('trinhtuantai89@gmail.com')->send(new TestEmail());
    }
}
