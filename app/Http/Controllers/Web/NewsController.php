<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

use App\News;
use App\NewsCategory;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug = null)
    {
        if($slug == null) {
            $category = null;
            $data = News::where('status',1)->where('locale',$this->locale)->paginate(20);

            $setting_seo = $this->getOption('seo');

            foreach (config('app.languages') as $key => $value) {
                $switch_languages[$key] = route('web.news_categories.'.$key);
            }

            $meta_seo = $this->meta_seo('',0,[
                'title' => $setting_seo['meta_title_news'] ?? 'Tiêu đề',
                'description'=> $setting_seo['meta_description_news'] ?? 'Mô tả',
            ],$switch_languages);

            $admin_bar_edit = route('admin.setting.seo');
            return view('web.news.index',compact('category','data','meta_seo','admin_bar_edit','switch_languages'));
        }else {
            $category = NewsCategory::where('status',1)->where('slug',$slug)->firstOrFail();
            //nó
            //dump($category);
            //cha nó
            //dump($category->getParent());
            //các con nó
            //dump($category->getChilds());
            
            $child_ids = $category->getChildIds();
            $data = News::where('status',1)->whereIn('category_id',$child_ids)->where('locale',$this->locale)->paginate(20);

            foreach (config('app.languages') as $key => $value) {
                if($key == $this->locale) {
                    $switch_languages[$key] = route('web.news_categories.'.$key,$category->slug);
                }else {
                    $locale_record = getLocaleRecord('news_categories',$category->id,$key,$category->from_record);
                    $switch_languages[$key] = $locale_record ? route('web.news_categories.'.$key,$locale_record->slug) : null;
                }
            }

            $meta_seo = $this->meta_seo('news_categories',$category->id,[
                'title' => $category->name ?? 'Tiêu đề',
                'description'=> cutString(removeHTML($category->detail),175) ?? 'Mô tả',
            ],$switch_languages);

            $admin_bar_edit = route('admin.setting.seo');
            return view('web.news.index',compact('category','data','meta_seo','admin_bar_edit','switch_languages'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $data = News::where('status',1)->where('slug',$slug)->firstOrFail();
        foreach (config('app.languages') as $key => $value) {
            if($key == $this->locale) {
                $switch_languages[$key] = route('web.news.'.$key,$data->slug);
            }else {
                $locale_record = getLocaleRecord('news',$data->id,$key,$data->from_record);
                $switch_languages[$key] = $locale_record ? route('web.news.'.$key,$locale_record->slug) : null;
            }
        }

        $meta_seo = $this->meta_seo('news',$data->id,[
            'title' => $data->name ?? 'Tiêu đề',
            'description'=> cutString(removeHTML($data->detail),175) ?? 'Mô tả',
        ],$switch_languages);

        $admin_bar_edit = route('news.edit',$data->id);
        return view('web.news.index',compact('data','meta_seo','admin_bar_edit','switch_languages'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
