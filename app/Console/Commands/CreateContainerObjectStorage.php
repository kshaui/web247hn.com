<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\MyClass\ObjectStorageService;

class CreateContainerObjectStorage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CreateContainerObjectStorage {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a container object storage openstack';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $config = config('filesystems.disks.sop');
        $service = new ObjectStorageService($config);
        $container = $service->createContainer($name);
        $container->saveMetadata([
            'X-Container-Read' => '.r:*'
        ],false);
        if($container !== false) {
            $this->info('Tao moi container thanh cong!');
        }else {
            $this->info('Co loi xay ra! Container da toan tai hoac khong the tao container');
        }
    }
}
