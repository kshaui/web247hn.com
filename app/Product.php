<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Attribute;
use DB;

class Product extends Model
{
    //
    public function attribute() {
        return $this->hasMany('App\Attribute','product_id');
    }

    public function attribute_value() {
        return DB::table('attributes')->where('product_id', $this->id)->list('key','value');
    }

    public function get_attribute($key) {
        $attribute =  $this->attribute()->where('key', $key)->first();
        if ($attribute)
            return $attribute->value;
        else
            return null;
    }

    public function set_attribute($key, $value) {
        $attribute = $this->attribute()->where('key', $key)->first();
        if (!$attribute) {
            $attribute = new Attribute;
            $attribute->product_id = $this->id;
            $attribute->key = $key;
        }
        $attribute->value = $value;
        $attribute->save();
    }
}
